package microservices.book.socialmultiplication.serviceImpl;

import microservices.book.socialmultiplication.service.RandomGeneratorService;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class RandomGeneratorServiceImpl implements RandomGeneratorService {

    private final static Long MINIMUM_FACTOR = 1L;
    private final static Long MAXIMUM_FACTOR = 100L;

    @Override
    public Long generateRandomFactor() {
        return new Random().longs(1L, MINIMUM_FACTOR, MAXIMUM_FACTOR).findFirst().getAsLong();
    }
}
