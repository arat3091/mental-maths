package microservices.book.socialmultiplication.serviceImpl;

import microservices.book.socialmultiplication.domain.Multiplication;
import microservices.book.socialmultiplication.domain.MultiplicationResultAttempt;
import microservices.book.socialmultiplication.service.MultiplicationService;
import microservices.book.socialmultiplication.service.RandomGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MultiplicationServiceImpl implements MultiplicationService {

    @Autowired
    private RandomGeneratorService randomGeneratorService;

    @Override
    public Multiplication createRandomMultiplication() {
        Long operand1 = randomGeneratorService.generateRandomFactor();
        Long operand2 = randomGeneratorService.generateRandomFactor();
        Long result = operand1 * operand2;
        return Multiplication.builder()
                .operand1(operand1)
                .operand2(operand2)
                .result(result)
                .build();
    }

    @Override
    public boolean checkAttempt(MultiplicationResultAttempt multiplicationResultAttempt) {
        return multiplicationResultAttempt.getMultiplication().getResult().equals(multiplicationResultAttempt.getResultAttempt());
    }
}
