package microservices.book.socialmultiplication.controller;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import microservices.book.socialmultiplication.domain.Multiplication;
import microservices.book.socialmultiplication.domain.MultiplicationResultAttempt;
import microservices.book.socialmultiplication.service.MultiplicationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("multiplications")
public class MultiplicationController {
    private final MultiplicationService multiplicationService;

    public MultiplicationController(final MultiplicationService multiplicationService) {
        this.multiplicationService = multiplicationService;
    }

    @GetMapping("random")
    public Multiplication getRandomMultiplication() {
        return multiplicationService.createRandomMultiplication();
    }

    @PostMapping("results")
    public Boolean postResults(@RequestBody MultiplicationResultAttempt multiplicationResultAttempt) {
        return multiplicationService.checkAttempt(multiplicationResultAttempt);
    }


}
