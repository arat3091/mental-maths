package microservices.book.socialmultiplication.service;

public interface RandomGeneratorService {
    Long generateRandomFactor();
}
