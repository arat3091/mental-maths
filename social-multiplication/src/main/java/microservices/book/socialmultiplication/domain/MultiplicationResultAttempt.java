package microservices.book.socialmultiplication.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MultiplicationResultAttempt {
    private final User user;
    private final Multiplication multiplication;
    private final Long resultAttempt;
}
