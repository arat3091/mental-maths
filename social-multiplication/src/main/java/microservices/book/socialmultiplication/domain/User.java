package microservices.book.socialmultiplication.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public final class User {
    private final String alias;
}
