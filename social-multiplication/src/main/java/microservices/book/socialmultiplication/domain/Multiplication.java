package microservices.book.socialmultiplication.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
public class Multiplication {
    private Long operand1;
    private Long operand2;
    private Long result;
}
