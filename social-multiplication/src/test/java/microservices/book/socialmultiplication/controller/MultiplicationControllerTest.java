package microservices.book.socialmultiplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import microservices.book.socialmultiplication.domain.Multiplication;
import microservices.book.socialmultiplication.domain.MultiplicationResultAttempt;
import microservices.book.socialmultiplication.domain.User;
import microservices.book.socialmultiplication.service.MultiplicationService;
import org.assertj.core.api.Assertions;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers=MultiplicationController.class,secure=false)
public class MultiplicationControllerTest {

    @MockBean
    public MultiplicationService multiplicationService;

    @Autowired
    public MockMvc mockMvc;

    public JacksonTester<Multiplication> multiplicationJacksonTester;
    public JacksonTester<MultiplicationResultAttempt> multiplicationResultAttemptJacksonTester;

    @Before
    public void setUp() {
        JacksonTester.initFields(this, new ObjectMapper());
    }

    @Test
    public void getRandomMultiplicationTest() throws Exception{
        BDDMockito.given(multiplicationService.createRandomMultiplication())
                .willReturn(Multiplication.builder()
                .operand1(1L)
                .operand2(5L)
                .result(5L)
                .build());

        MockHttpServletResponse mockHttpServletResponse =
                mockMvc.perform(MockMvcRequestBuilders.get("/multiplications/random")
                .accept(MediaType.APPLICATION_JSON)
                ).andReturn().getResponse();

        Assertions.assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpStatus.OK.value());
        Assertions.assertThat(mockHttpServletResponse.getContentAsString())
                .isEqualTo(multiplicationJacksonTester.write(Multiplication.builder()
                .operand1(1L)
                .operand2(5L)
                .result(5L).build()).getJson());
    }

    @Test
    public void postResults() throws Exception{
        BDDMockito.given(multiplicationService.checkAttempt(any(MultiplicationResultAttempt.class)))
                .willReturn(true);

        MockHttpServletResponse mockHttpServletResponse =
                mockMvc.perform(MockMvcRequestBuilders.post("/multiplications/results")
                .contentType(MediaType.APPLICATION_JSON)
                .content(multiplicationResultAttemptJacksonTester.write(MultiplicationResultAttempt.builder().build()).getJson()))
                .andReturn().getResponse();

        Assertions.assertThat(mockHttpServletResponse.getStatus()).isEqualTo(HttpStatus.OK.value());
    }
}