package microservices.book.socialmultiplication.service;

import microservices.book.socialmultiplication.serviceImpl.RandomGeneratorServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class RandomGeneratorServiceTest {
    private RandomGeneratorService randomGeneratorService;

    @Before
    public void setUp() {
        randomGeneratorService = new RandomGeneratorServiceImpl();
    }

    @Test
    public void generateRandomFactor() {
        List<Long> list = LongStream.range(0L, 1000L)
                .map(i -> randomGeneratorService.generateRandomFactor())
                .boxed()
                .collect(Collectors.toList());
        Assertions.assertThat(list).containsOnlyElementsOf(LongStream.range(1L, 100L).boxed().collect(Collectors.toList()));
    }

}