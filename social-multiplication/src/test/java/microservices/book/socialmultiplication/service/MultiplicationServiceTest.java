package microservices.book.socialmultiplication.service;

import microservices.book.socialmultiplication.domain.Multiplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MultiplicationServiceTest {
    @MockBean
    private RandomGeneratorService randomGeneratorService;

    @Autowired
    private MultiplicationService multiplicationService;

    @Test
    public void createMultiplicationTest(){
        BDDMockito.given(randomGeneratorService.generateRandomFactor()).willReturn(50L,30L);
        Multiplication multiplication = multiplicationService.createRandomMultiplication();

        assertThat(multiplication.getOperand1()).isEqualTo(50L);
        assertThat(multiplication.getOperand2()).isEqualTo(30L);
        assertThat(multiplication.getResult()).isEqualTo(1500L);
    }

}